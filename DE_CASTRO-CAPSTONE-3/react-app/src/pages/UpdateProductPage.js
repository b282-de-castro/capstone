import React, { useState, useEffect } from 'react';
import { useNavigate, Link, useParams } from 'react-router-dom'; 
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

export default function UpdateProductPage() {
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const {productId} = useParams();


useEffect(() => {
    // Fetch product details from the server based on the productId
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: 'GET',
    })
      .then((res) => res.json())
      .then((data) => {
        // Check if data is valid and not empty
        if (data && Object.keys(data).length > 0) {
          console.log(data)
          setName(data.name);
          setDescription(data.description);
          setPrice(data.price.toString());
        } else {
          // Handle the case where the data is not valid or empty
          console.error("Invalid or empty product data received.");
        }
      })
      .catch((error) => {
        console.error("Error fetching product details:", error);
        // Handle the error, show an error message, or redirect to an error page
      });
  }, [productId]);

  function updateProduct(e) {
    e.preventDefault();

    const productData = {
      'name': name,
      'description':description,
      'price': parseFloat(price),
    };

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(productData),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          setName("");
          setDescription("");
          setPrice("");

          Swal.fire({
            title: "Product Updated",
            icon: "success",
            text: "Product has been updated.",
          });

          navigate("/admin_dashboard");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again.",
          });
        }
      })
      .catch((error) => {
        console.error("Error updating product:", error);

        // Handle the error, show an error message, or redirect to an error page
        Swal.fire({
          title: "Error",
          icon: "error",
          text: "Something went wrong. Please try again later.",
        });
      });
  }

  return (
    <div className="container mt-5">
      <h1 className="text-center">Update Product</h1>
      <Form onSubmit={updateProduct}>
        <Form.Group controlId="productName">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="productDescription">
          <Form.Label>Product Description</Form.Label>
          <Form.Control
            as="textarea"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="productPrice">
          <Form.Label>Product Price</Form.Label>
          <Form.Control
            type="number"
            step="0.01"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Update Product
        </Button>
      </Form>
    </div>
  );
}
