import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

  const data = {
    title: "Maki's Online Pet Necessities",
    content: "We got all your Pet's needs!",
    destination: "/products",
    label: "Check it out!"
  }


  return (
    <>
    <Banner data={data} />
      <Highlights />
      
    </>
  )
}