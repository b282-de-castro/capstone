
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import {useState, useContext} from 'react';

import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';

function AppNavbar() {

    const {user} = useContext(UserContext);

  return (

    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand as={Link} to="/">Maki's Online Pet Necessities</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>

            { (user.id) ?
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                :
                <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                </>
              }
            
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

  );
}

export default AppNavbar;