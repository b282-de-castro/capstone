import React from 'react';
import { Button, Row, Col, Card } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';

export default function AdminProductPage({ product, setProducts, products }) {
  const { name, description, price, _id, isActive } = product;
  const {productId} = useParams();

  const archiveProduct = () => {
    
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({ 
        'isAdmin': true,

      })
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.value === true) {
          setProducts((prevProducts) =>
            prevProducts.map((p) => (p._id === _id ? { ...p, isActive: false } : p))
          );
        }
      })
      .catch((error) => {
        console.error('Error archiving the product:', error);
      });
  };

  const activateProduct = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({ 
        isAdmin: true 
      })
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          setProducts((prevProducts) =>
            prevProducts.map((p) => (p._id === _id ? { ...p, isActive: true } : p))
          );
        }
      })
      .catch((error) => {
        console.error('Error activating the product:', error);
      });
  };

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title>
              <h4>{name}</h4>
            </Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Button className="bg-primary" as={Link} to={`/products/${_id}/update`}>
              Update
            </Button>
            {/* Add a button to toggle the isActive property */}
            <Button
              variant={isActive ? 'success' : 'danger'}
              onClick={isActive ? archiveProduct : activateProduct}
            >
              {isActive ? 'Active' : 'Archived'}
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
