import{Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Fresh and Nutritious Food for your Furbabies</h2>
	                    </Card.Title>
	                    <Card.Text>
	                      Introducing our line of fresh and nutritious pet food products, designed to cater to the unique dietary needs of your beloved furry companions. We understand the importance of providing wholesome and balanced meals that contribute to their overall health and well-being.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Fashionable and Comfortable Pet Clothing</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Introducing our stylish and comfortable line of pet clothes, specially designed to keep your furry friends looking adorable and feeling cozy all year round. Dress your pets in our fashionable and comfortable pet clothes, and witness the joy they bring to everyone around them. Whether it's a casual outing or a special event, our pet clothing collection has something for every occasion. Let your pets strut their stuff with confidence and flair!
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Explore our pet Online Community</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Choose from our wide range of premium pet products to enrich your pet's life and strengthen the bond between you and your beloved companion. Each product is thoughtfully designed to meet your pet's needs and enhance their overall well-being.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}

